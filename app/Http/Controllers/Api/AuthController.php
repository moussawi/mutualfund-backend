<?php

namespace App\Http\Controllers\Api;

use DB;
use Auth;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    
    public function postLogin(Request $request){        
        
        $username = $request->input('username');
        $password = $request->input('password');
        $rememberMe = (bool) $request->input('remember');
        
        $count = DB::connection('teachers')
                ->table('PasswordTables')
                ->where([
                    'TeaID' => $username,
                    'Password' => $password,
                ])                      
                ->count();
                
        if($count === 1) {
            
            $user = User::find($username);
            
            // dd($user);
            
            \Log::info("[AUTH] Login user {$user->TeaID} {$user->FirstName}");
            
            Auth::login($user, $rememberMe);
            
           return [
               'user' => $user
            ];
            
        }
        
        return response([
            'status' => 'FAILED'
        ], 403);
                
    }
    
    public function postLogout() {
        
        Auth::logout();
        
        return [];
        
    }
   
}
