<?php

namespace App\Http\Controllers\Api;

use DB;
use Auth;
use Cache;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{   
    function __construct() {
        $this->middleware('auth');
    } 
    
    public function getIndex(Request $request) {  
        
        // return file_get_contents(storage_path('app/response.json'));
              
        
        $data = Cache::remember("user_{Auth::id()}", 30, function() use($request) {
            $user = $request->user();
            
            $user->load('Beneficiaries.Relation');
            $user->load('PharmacyTreatments.Pharmacy');
            $user->load('LabTreatments.Lab');
            $user->load('Beneficiaries.ChronicDiseases.Disease');
            
            $user->load('Branch.Faculty'); 
            
            $user->load('MedicalAid.Decree.Assignments');
            $user->load('FinancialAid.Decree.Assignments');
            
            return $user;            
        });
        
        

                      
        return $data;
        
    }     

}
