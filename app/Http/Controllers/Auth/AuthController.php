<?php

namespace App\Http\Controllers\Auth;

use DB;
use Auth;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    
    public function getLogin(){        
        return view("auth.login");        
    }
    
    public function postLogin(Request $request){        
        
        $username = $request->input('username');
        $password = $request->input('password');
        $rememberMe = (bool) $request->input('remember');
        
        $count = DB::connection('teachers')
                ->table('PasswordTables')
                ->where([
                    'TeaID' => $username,
                    'Password' => $password,
                ])                      
                ->count();
                
        if($count === 1) {
            
            $user = User::find($username);
            
            // dd($user);
            
            \Log::info("[AUTH] Login user {$user->TeaID} {$user->FirstName}");
            
            Auth::login($user, $rememberMe);
            
            
            return redirect()->intended('/');
            
        }
        
        return view('auth.login');
                
    }
    
    public function getLogout() {
        
        Auth::logout();
        
        return redirect('/');
        
    }
   
}
