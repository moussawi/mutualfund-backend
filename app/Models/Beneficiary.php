<?php

namespace App\Models;

use Illuminate\Database\ELoquent\Model;

class Beneficiary extends Model
{
    protected $primaryKey = 'Id';
    
    protected $table = 'TeaFam';
    
    protected $connection = 'teachers';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
    
    public function Relation() {
        return $this->hasOne('App\Models\Relation', 'Id', 'RelationID');
    }
    
    public function ChronicDiseases() {
        return $this->hasMany('App\Models\Chronic\DiseaseAffection', 'IDDuPersonMalade', 'Id');
    }
    
}
