<?php

namespace App\Models\Chronic;

use Illuminate\Database\ELoquent\Model;

class Disease extends Model
{
    protected $primaryKey = 'Id';
    
    protected $table = 'CodeMalade';
    
    protected $connection = 'chronic';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'upsize_ts'
    ];
           
}
