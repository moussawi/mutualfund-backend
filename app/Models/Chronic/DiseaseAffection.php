<?php

namespace App\Models\Chronic;

use Illuminate\Database\ELoquent\Model;
/**
 * 
 * Affected Beneficairies records
 *
 */
class DiseaseAffection extends Model
{
    protected $primaryKey = 'ID';
    
    protected $table = 'TeaMalade';
    
    protected $connection = 'chronic';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'upsize_ts'
    ];

    public function Disease(){
        return $this->hasOne('App\Models\Chronic\Disease', 'Id', 'CodeMalade')
            ->join('CodeMaladeBaseType', 'CodeMaladeBaseType.Id', '=', 'CodeMalade.TypeID')
            ->select([                
                'CodeMalade.Id as Id',
                'CodeMalade.Description as Name',
                'CodeMaladeBaseType.Description as Type'
            ]);
    }   
    
}
