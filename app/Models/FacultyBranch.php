<?php

namespace App\Models;

use Illuminate\Database\ELoquent\Model;

class FacultyBranch extends Model
{
    protected $primaryKey = 'Id';
    
    protected $table = 'FacultyBranches';
    
    protected $connection = 'teachers';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'upsize_ts'
    ];
    
    public function Faculty(){
        return $this->belongsTo('App\Models\Faculty', 'FacultyID', 'Id');
    }
    
}
