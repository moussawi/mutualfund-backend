<?php

namespace App\Models\Financial;

use Illuminate\Database\ELoquent\Model;

class Aid extends Model
{
    protected $primaryKey = 'ID';
    
    protected $table = 'Minah';
    
    protected $connection = 'financial_aid';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'upsize_ts'
    ];
    
    public function User(){
        return $this->hasOne('App\Models\User', 'TeaID', 'TEACH_NO');
    }

    public function Decree(){
        return $this->hasOne('App\Models\Medical\Decree', 'n_karar', 'Karar_n');
    }   
    
}
