<?php

namespace App\Models\Financial;

use Illuminate\Database\ELoquent\Model;

class Assignment extends Model
{
    protected $primaryKey = 'ID';
    
    protected $table = 'Ihalat';
    
    protected $connection = 'financial_aid';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
    
}
