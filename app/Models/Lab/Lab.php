<?php

namespace App\Models\Lab;

use Illuminate\Database\ELoquent\Model;

class Lab extends Model
{
    protected $primaryKey = 'LABNO';
    
    protected $table = 'labtbl';
    
    protected $connection = 'lab';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'upsize_ts'
    ];
    
}
