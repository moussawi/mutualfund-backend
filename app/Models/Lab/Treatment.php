<?php

namespace App\Models\Lab;

use Illuminate\Database\ELoquent\Model;

class Treatment extends Model
{
    public $incrementing = false; 
    
    protected $primaryKey = 'REFRENCE';
    
    protected $table = 'OUTDTR';
    
    protected $connection = 'lab';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'upsize_ts'
    ];
    
    public function User(){
        return $this->hasOne('App\Models\User', 'TeaID', 'TEACH_NO');
    }
    
    public function Lab(){
        return $this->hasOne('App\Models\Lab\Lab', 'LABNO', 'HOSNBR');
    } 
    
}
