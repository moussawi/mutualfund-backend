<?php

namespace App\Models\Medical;

use Illuminate\Database\ELoquent\Model;

class Decree extends Model
{
    protected $primaryKey = 'id_karrar';
    
    protected $table = 'id_kararat';
    
    protected $connection = 'medical_aid';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'upsize_ts'
    ];
    
    public function Assignments() {
        return $this->hasMany('App\Models\Medical\DecreeAssignment', 'IdKarar', 'n_karar')
            ->join('Ihalat', 'Ihalat.IdIhala', '=', 'IhalaltKararat.Idihala')
            ->orderBy('DateIhala', 'DESC');
    }
    
}
