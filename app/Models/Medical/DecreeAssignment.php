<?php

namespace App\Models\Medical;

use Illuminate\Database\ELoquent\Model;

class DecreeAssignment extends Model
{
    protected $primaryKey = 'id';
    
    protected $table = 'IhalaltKararat';
    
    protected $connection = 'medical_aid';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
    
}
