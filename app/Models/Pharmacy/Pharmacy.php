<?php

namespace App\Models\Pharmacy;

use Illuminate\Database\ELoquent\Model;

class Pharmacy extends Model
{
    protected $primaryKey = 'CODE1';
    
    protected $table = 'z_code_pharmacy';
    
    protected $connection = 'pharmacy';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'upsize_ts'
    ];
    
}
