<?php

namespace App\Models\Pharmacy;

use Illuminate\Database\ELoquent\Model;

class Treatment extends Model
{
    public $incrementing = false; 
    
    protected $primaryKey = 'REFRENCE';
    
    protected $table = 'pharmacy';
    
    protected $connection = 'pharmacy';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'upsize_ts'
    ];
    
    public function User(){
        return $this->hasOne('App\Models\User', 'TeaID', 'TEACH_NO');
    }
    
    public function Pharmacy(){
        return $this->hasOne('App\Models\Pharmacy\Pharmacy', 'CODE1', 'PhrNbr');
    } 
    
}
