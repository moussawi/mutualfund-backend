<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $primaryKey = 'TeaID';
    
    protected $table = 'Tea';
    
    protected $connection = 'teachers';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'upsize_ts',
        'Password',
        'remember_token',
    ];
    
    public function Beneficiaries() {
        return $this->hasMany('App\Models\Beneficiary', 'TeaID');
    }
    
    public function Branch() {
        return $this->belongsTo('App\Models\FacultyBranch', 'FacultyBId');
    }
    
    public function MedicalAid() {
        return $this->hasMany('App\Models\Medical\Aid', 'TEACH_NO', 'TeaID');
    }

    public function FinancialAid() {
        return $this->hasMany('App\Models\Financial\Aid', 'TEACH_NO', 'TeaID');
    }

    public function PharmacyTreatments() {
        return $this->hasMany('App\Models\Pharmacy\Treatment', 'TEACH_NO', 'TeaID'); //->join('z_code_pharmacy', 'z_code_pharmacy.CODE1', '=', 'pharmacy.PhrNbr');
    }

    public function LabTreatments() {
        return $this->hasMany('App\Models\Lab\Treatment', 'TEACH_NO', 'TeaID'); //->join('z_code_pharmacy', 'z_code_pharmacy.CODE1', '=', 'pharmacy.PhrNbr');
    }
    
}
